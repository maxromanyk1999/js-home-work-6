"use strict"

/*
1.  Створіть об'єкт product з властивостями name, price та discount.
Додайте метод для виведення повної ціни товару з урахуванням знижки.
Викличте цей метод та результат виведіть в консоль.
 */


const product = {
    name: 'chips',
    price: 47,
    discount: 10,
    getDiscountProduct : function (price, discount){
        let num = (this.price / 100) * this.discount;
        this.price = this.price - num;
        return this.price;
    }
}

console.log(`Товар зі знижкою "${product['name']}" - `,product.getDiscountProduct());



/*
2. Напишіть функцію, яка приймає об'єкт з властивостями name та age, і повертає рядок з привітанням і віком,
наприклад "Привіт, мені 30 років". Попросіть користувача ввести своє ім'я та вік
за допомогою prompt, і викличте функцію з введеними даними.
 Результат виклику функції виведіть з допомогою alert.
 Результат виклику функції виведіть з допомогою alert.
*/

let name = prompt("Введіть своє ім'я");
let age = prompt("Введіть свій вік");

while (typeof name !== 'string' || !isNaN(name)) {
    name = prompt("Введіть своє ім'я ще раз");
}
while (isNaN(age) || age <= 8 || age > 100){
    age = prompt("Введість ще раз свій вік");
}

const user = {
    name,
    age,
};

const getObjectFunk = (obj) => {
    return alert(`Привіт, мене звати ${user['name']}, мені ${user['age']} age.`);
}
getObjectFunk(user);

/*
3. Реалізувати повне клонування об'єкта.
 */


const user1 = {
    name: 'Dima',
    age: 30,
    hobby: {
        game: "DOTA",
    },
    telephone: 3800323242,
}

const deepCloneObj = (object) => {

    const cloneObj = {};

    for (let key in object) {

        if (typeof object[key] === 'object') {
            cloneObj[key] = deepCloneObj(object[key]);
        } else {
            cloneObj[key] = object[key];
        }
    }
    return cloneObj;
}

const user2 = deepCloneObj(user1);

console.log(user1);
console.log(deepCloneObj(user2));


// неповне клонування
/*const user2 = {}
Object.assign(user2, user1);

console.log(user1);
console.log(user2);*/




